package com.experis;

import java.util.ArrayList;
import java.util.Scanner;

public class Program {

    /**
     * Searches for match(es) for the given input in the list of contacts
     * @param input the name/number to search for
     * @param list the list in which to search for match(es)
     */
    private static void searchList(String input, ArrayList<Contact> list) {
        ArrayList<Contact> listOfMatches = null;

            for (Contact contact : list) {
                if (contact.toString().contains(input)) {
                    // Create the list only if there are any matches found
                    if (listOfMatches == null) {
                        listOfMatches = new ArrayList<>();
                    }
                    listOfMatches.add(contact);
                }
            }

        if (listOfMatches != null) {
            printContact(listOfMatches, input);
        } else {
            System.out.println("No person found");
        }
    }

    /**
     * Prints contact information for every contact in the given ArrayList
     * @param list the list of contacts to print out
     */
    private static void printContact(ArrayList<Contact> list, String input) {
        final String ANSI_YELLOW = "\u001B[33m";
        final String ANSI_RESET = "\u001B[0m";

        for(Contact contact : list) {
            String str = contact.toString();
            System.out.println(str.replace(input,ANSI_YELLOW + input + ANSI_RESET) + "\n");
        }
    }

    public static void main(String[] args) {
        ArrayList<Contact> peopleList = new ArrayList<>();
        peopleList.add(new Contact("Jessica Lo", "Blueberry St. 5", "0704482674", "2000/12/28"));
        peopleList.add(new Contact("Tina Berg", "Apple St. 12", "0737313943", "1992/01/05"));
        peopleList.add(new Contact("Jessica Quach", "Apple St. 1", "+4637313945", "1998/04/25"));
        peopleList.add(new Contact("Anki Duck", "Pear St. 2", "0762458375", "+4537773842", "1990/03/06"));
        peopleList.add(new Contact("Bjorn Bjornsson", "Potato St. 13", "+4043552564", "+403525475", "1984/08/19"));
        peopleList.add(new Contact("Pelle Svanslos", "Unknown Street", "0736782302", "2008/11/29"));
        peopleList.add(new Contact("Luke Prick", "MyStreet 2", "0738247263", "2008/11/19"));
        peopleList.add(new Contact("Mona Lala", "Berry Road 3b", "3584758349", "2008/11/29"));
        peopleList.add(new Contact("Olof Snowman", "Berry Road 2A", "0736782302", "2008/11/29"));
        peopleList.add(new Contact("Heihei Lef", "Banana Road 2", "847285637", "2001/11/29"));
        peopleList.add(new Contact("Lisa Storm", "Main St. 11", "+11123344", "0035883344", "1899/01/09"));
        peopleList.add(new Contact("Samwise Gamgi", "Potato St. 41", "0074783647", "+24938473", "1765/09/06"));


        Scanner scanner = new Scanner(System.in);
        String input;
        // Run until user inputs something "correct"
        while(true) {
            System.out.println("Enter something to search through the contacts list.");
            input = scanner.next();
            System.out.println("User input: " + input);
            if(!input.isBlank()) {
                break;
            }
        }
        // Search for input phrase in the list of contacts
        searchList(input, peopleList);
    }

}
