package com.experis;

/**
 * TODO: comments, change bday to date, check if empty
 * EXTRA: contacts in .txt, .json
 * This class represents a com.experis.Contact and holds information such as name, address and phone numbers.
 */
public class Contact {
    private String name, address, mobileNr, workNr, bday;

    /**
     * Constructor including work number. Used when creating a com.experis.Contact that has a work number
     * @param name the name of the contact
     * @param address the address of the contact
     * @param mobileNr the mobile number of the contact
     * @param workNr the work number of the contact
     * @param bday the birthday of the contact
     */
    public Contact(String name, String address, String mobileNr, String workNr, String bday) {
        this.name = name;
        this.address = address;
        this.mobileNr = mobileNr;
        this.workNr = workNr;
        this.bday = bday;
    }

    /**
     * Constructor excluding work number. Used when creating a com.experis.Contact without a work number
     * @param name the name of the contact
     * @param address the address of the contact
     * @param mobileNr the mobile number of the contact
     * @param bday the birthday of the contact
     */
    public Contact(String name, String address, String mobileNr, String bday) {
        this.name = name;
        this.address = address;
        this.mobileNr = mobileNr;
        this.bday = bday;
        workNr = "";
    }

    /**
     * Returns the name of the contact
     * @return the contact's name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the contact's mobile number
     * @return the mobile number
     */
    public String getMobileNr() {
        return mobileNr;
    }

    /**
     * Returns the contact's work number
     * @return the work number
     */
    public String getWorkNr() {
        return workNr;
    }


    /**
     * Returns full detail about the contact
     * @return a String with contact information
     */
    @Override
    public String toString() {

        StringBuilder str = new StringBuilder();
        str.append("Name: ").append(name);
        str.append("\nAddress: ").append(address);
        str.append("\nDate of birth: ").append(bday);
        str.append("\nMobile number: ").append(mobileNr);
        if(workNr.isBlank()) {
            workNr = "Not registered";
        }
        str.append("\nWork number: ").append(workNr);
        return str.toString();
    }
}